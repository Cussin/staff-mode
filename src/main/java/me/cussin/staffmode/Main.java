package me.cussin.staffmode;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/*
Have fun xd
 */

@Getter
public class Main extends JavaPlugin {

    @Getter
    private FrozenManager frozenManager;
    @Getter
    private StaffModeManager staffModeManager;

    @Override
    public void onEnable() {
        instance = this;

        saveDefaultConfig();

        ConfigurationSerialization.registerClass(InventoryData.class);
        getCommand("staff").setExecutor(this);
        frozenManager = new FrozenManager();
        staffModeManager = new StaffModeManager();
        getServer().getPluginManager().registerEvents(frozenManager, this);
        getServer().getPluginManager().registerEvents(staffModeManager, this);
    }

    @Override
    public void onDisable() {
        staffModeManager.saveData();
    }

    private static Main instance;

    public static Main getInstance() {
        return instance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase("staff") && sender instanceof Player && sender.hasPermission("staff.use")) {
            Player p = (Player) sender;
            if (staffModeManager.staffList.contains(p.getUniqueId())) {
                staffModeManager.staffList.remove(p.getUniqueId());
                staffModeManager.getInventoryMap().get(p.getUniqueId()).applyTo(p);
                staffModeManager.getInventoryMap().remove(p.getUniqueId());
                p.setGameMode(GameMode.SURVIVAL);
                p.removePotionEffect(PotionEffectType.NIGHT_VISION);
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.showPlayer(p);
                }
                p.sendMessage(ChatColor.WHITE + "Staff mode " + ChatColor.RED + "disabled");
                return true;
            }
            staffModeManager.getInventoryMap().put(p.getUniqueId(), new InventoryData(p));
            staffModeManager.staffList.add(p.getUniqueId());
            p.getInventory().clear();
            p.setGameMode(GameMode.CREATIVE);
            p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 3));
            ItemStack blazeRod = new ItemStack(Material.BLAZE_ROD);
            ItemMeta meta = blazeRod.getItemMeta();
            meta.setDisplayName(ChatColor.RED + "Teleport");
            blazeRod.setItemMeta(meta);
            ItemStack apple = new ItemStack(Material.APPLE);
            meta = apple.getItemMeta();
            meta.setDisplayName(ChatColor.GREEN + "Open Inventory");
            apple.setItemMeta(meta);
            ItemStack iceBlock = new ItemStack(Material.ICE);
            meta = iceBlock.getItemMeta();
            meta.setDisplayName(ChatColor.AQUA + "Freeze");
            iceBlock.setItemMeta(meta);
            ItemStack compass = new ItemStack(Material.COMPASS);
            meta = compass.getItemMeta();
            meta.setDisplayName(ChatColor.YELLOW + "Info");
            compass.setItemMeta(meta);
            ItemStack unvanishDye = new ItemStack(Material.INK_SACK, 1, (byte) 10);
            meta = unvanishDye.getItemMeta();
            meta.setDisplayName(ChatColor.GREEN + "Vanished");
            unvanishDye.setItemMeta(meta);
            p.getInventory().setItem(0, blazeRod);
            p.getInventory().setItem(2, apple);
            p.getInventory().setItem(4, iceBlock);
            p.getInventory().setItem(6, compass);
            p.getInventory().setItem(8, unvanishDye);
            p.updateInventory();
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (!staffModeManager.getStaffList().contains(player.getUniqueId())) {
                    player.hidePlayer(p);
                }
            }
            p.sendMessage(ChatColor.WHITE + "Staff mode " + ChatColor.GREEN + "enabled");
            return true;
        } else {
            sender.sendMessage(ChatColor.RED + "No permission");
        }
        return false;
    }
}
