package me.cussin.staffmode;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

/*
 * Copyright (c) 2016 by poisonex.
 * To use ANY of this code, you MUST first receive proper permission from
 * me (poisonex), which is usually a typed document stating the words
 * "You have permission to use this set of code in your own projects."
 */

public class ExternalConfiguration extends YamlConfiguration {
    private final JavaPlugin plugin;
    private final File file;

    public ExternalConfiguration(JavaPlugin plugin, String fileName) {
        this(plugin, fileName, true);
    }

    public ExternalConfiguration(JavaPlugin plugin, String fileName, boolean shouldLoad) {
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder(), fileName);
        if (shouldLoad) {
            load();
        }
    }

    public File getFile() {
        return this.file;
    }

    public JavaPlugin getPlugin() {
        return this.plugin;
    }

    public void clear() {
        map.clear();
    }

    public void load() {
        try {
            if (!this.file.exists()) {
                if (this.plugin.getResource(this.file.getName()) != null) {
                    this.plugin.saveResource(this.file.getName(), false);
                } else {
                    super.save(this.file);
                }
            } else {
                super.load(this.file);
            }
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            save(this.file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
