package me.cussin.staffmode;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InventoryData implements ConfigurationSerializable {

    private ItemStack[] armorContents;
    private ItemStack[] contents;

    public InventoryData(Player player) {
        armorContents = player.getInventory().getArmorContents();
        contents = player.getInventory().getContents();
    }

    public InventoryData(Map<String, Object> map) {
        List<ItemStack> armorContentsList = (List<ItemStack>) map.get("armor");
        armorContents = armorContentsList.toArray(new ItemStack[armorContentsList.size()]);

        List<ItemStack> contentsList = (List<ItemStack>) map.get("contents");
        contents = contentsList.toArray(new ItemStack[contentsList.size()]);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();

        map.put("armor", Arrays.asList(armorContents));
        map.put("contents", Arrays.asList(contents));

        return map;
    }

    void applyTo(Player player) {
        player.getInventory().setContents(contents);
        player.getInventory().setArmorContents(armorContents);
        player.updateInventory();
    }
}
