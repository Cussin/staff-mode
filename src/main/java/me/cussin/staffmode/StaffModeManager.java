package me.cussin.staffmode;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCreativeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class StaffModeManager implements Listener {

    @Getter
    ArrayList<UUID> staffList = new ArrayList<>();
    @Getter
    private HashMap<UUID, InventoryData> inventoryMap = new HashMap<>();
    @Getter
    private ArrayList<UUID> hiddenList = new ArrayList<>();
    private ExternalConfiguration config;

    private static final String ITEM_TELEPORT = ChatColor.RED + "Teleport";
    private static final String ITEM_OPEN_INVENTORY = ChatColor.GREEN + "Open Inventory";
    private static final String ITEM_FREEZE = ChatColor.AQUA + "Freeze";
    private static final String ITEM_INFO = ChatColor.YELLOW + "Info";
    private static final String ITEM_UNVANISH = ChatColor.GRAY + "Visible";
    private static final String ITEM_VANISH = ChatColor.GREEN + "Vanished";

    //region Data Handling
    public StaffModeManager() {
        config = new ExternalConfiguration(Main.getInstance(), "staffdata.yml", false);
        loadData();
    }

    public void loadData() {
        config.load();

        staffList.clear();
        inventoryMap.clear();
        for (String key : config.getKeys(false)) {
            UUID uuid = UUID.fromString(key);
            staffList.add(uuid);
            inventoryMap.put(uuid, (InventoryData) config.get(key));
        }
    }

    public void saveData() {
        config.clear();

        for (Map.Entry<UUID, InventoryData> entry : inventoryMap.entrySet()) {
            config.set(entry.getKey().toString(), entry.getValue());
        }

        config.save();
    }
    //endregion

    //region Player Damage Event
    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent e) {
        if (staffList.contains(e.getDamager().getUniqueId())) {
            e.setCancelled(true);
            e.getDamager().sendMessage(ChatColor.RED + "You may not deal damage in staff mode");
        } else if (staffList.contains(e.getEntity().getUniqueId())) {
            e.setCancelled(true);
        }
    }
    //endregion

    //region Block Place Event
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (staffList.contains(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
        }
    }
    //endregion

    //region Block Break Event
    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (staffList.contains(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
        }
    }
    //endregion

    //region Creative Inventory Check
    @EventHandler
    public void onCreativeGrab(InventoryCreativeEvent e) {
        if (staffList.contains(e.getWhoClicked().getUniqueId()) && (!e.getWhoClicked().isOp()
                || !e.getWhoClicked().hasPermission("staff.creative"))) {
            e.setCancelled(true);
        }
    }
    //endregion

    //region Item Drop Event
    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        if (staffList.contains(e.getPlayer().getUniqueId()) && (!e.getPlayer().isOp() || e.getPlayer().hasPermission("staff.use"))) {
            e.setCancelled(true);
        }
    }
    //endregion

    //region Interact Event
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (!staffList.contains(e.getPlayer().getUniqueId()) && e.getPlayer().getItemInHand() != null && e.getPlayer().getItemInHand().hasItemMeta()) {
            ItemMeta meta = e.getPlayer().getItemInHand().getItemMeta();
            if (meta.hasDisplayName()) {
                String name = meta.getDisplayName();
                if (name.equalsIgnoreCase(ITEM_FREEZE) || name.equalsIgnoreCase(ITEM_OPEN_INVENTORY) || name.equalsIgnoreCase(ITEM_TELEPORT)) {
                    e.getPlayer().getInventory().setItemInHand(new ItemStack(Material.AIR));
                    e.getPlayer().updateInventory();
                    e.getPlayer().sendMessage(ChatColor.RED + "Staff item removed!");
                }
            }
        } else if (staffList.contains(e.getPlayer().getUniqueId())) {
            if (e.getPlayer().getItemInHand().hasItemMeta()) {
                ItemMeta meta = e.getPlayer().getItemInHand().getItemMeta();
                if (meta.hasDisplayName()) {
                    String name = meta.getDisplayName();
                    if (name.equalsIgnoreCase(ITEM_TELEPORT)) {
                        ArrayList<Player> players = new ArrayList<>(Bukkit.getOnlinePlayers());
                        players.remove(e.getPlayer());
                        if (players.isEmpty()) {
                            e.getPlayer().sendMessage(ChatColor.RED + "No players online!");
                            return;
                        }
                        Player randomPlayer = players.get(new Random().nextInt(players.size()));
                        if (staffList.contains(randomPlayer.getUniqueId())) {
                            e.getPlayer().sendMessage(ChatColor.RED + "Attempt to teleport to staff member " + randomPlayer.getDisplayName() + " cancelled!");
                        } else {
                            e.getPlayer().teleport(randomPlayer);
                            e.getPlayer().sendMessage(ChatColor.GREEN + "Teleported to " + randomPlayer.getDisplayName());
                        }
                    }
                    if (name.equalsIgnoreCase(ITEM_UNVANISH)) {
                        hiddenList.remove(e.getPlayer().getUniqueId());
                        ItemStack vanishDye = new ItemStack(Material.INK_SACK, 1, (byte) 10);
                        meta = vanishDye.getItemMeta();
                        meta.setDisplayName(ITEM_VANISH);
                        vanishDye.setItemMeta(meta);
                        e.getPlayer().getInventory().setItem(8, vanishDye);
                        for (Player player : Bukkit.getOnlinePlayers()) {
                            if (!staffList.contains(player.getUniqueId())) {
                                player.hidePlayer(e.getPlayer());
                            }
                        }
                        e.getPlayer().sendMessage(ChatColor.GRAY + "You have been" + ChatColor.GREEN + " Vanished");
                    } else if (name.equalsIgnoreCase(ITEM_VANISH)) {
                        hiddenList.add(e.getPlayer().getUniqueId());
                        ItemStack unvanishDye = new ItemStack(Material.INK_SACK, 1, (byte) 8);
                        meta = unvanishDye.getItemMeta();
                        meta.setDisplayName(ITEM_UNVANISH);
                        unvanishDye.setItemMeta(meta);
                        e.getPlayer().getInventory().setItem(8, unvanishDye);
                        for (Player player : Bukkit.getOnlinePlayers()) {
                            player.showPlayer(e.getPlayer());
                        }
                        e.getPlayer().sendMessage(ChatColor.GRAY + "You are now" + ChatColor.RED + " Visible");
                    }
                }
            }
        }
    }
    //endregion

    //region Entity Interact Event
    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractEntityEvent e) {
        if (!e.getPlayer().getItemInHand().hasItemMeta()) {
            return;
        }
        ItemMeta meta = e.getPlayer().getItemInHand().getItemMeta();
        if (e.getRightClicked() == null || !(e.getRightClicked() instanceof Player)) {
            return;
        }
        if (staffList.contains(e.getRightClicked().getUniqueId()) && staffList.contains(e.getPlayer().getUniqueId()) && meta.getDisplayName().equalsIgnoreCase(ITEM_FREEZE)) {
            e.getPlayer().sendMessage(ChatColor.RED + "You cannot freeze other staff members!");
            return;
        }

        Player rightClicked = (Player) e.getRightClicked();

        if (staffList.contains(e.getPlayer().getUniqueId())) {
            if (meta.hasDisplayName()) {
                String name = meta.getDisplayName();
                if (name.equalsIgnoreCase(ITEM_OPEN_INVENTORY)) {
                    e.getPlayer().openInventory(rightClicked.getInventory());
                } else if (name.equalsIgnoreCase(ITEM_FREEZE)) {
                    List<UUID> frozenList = Main.getInstance().getFrozenManager().getFrozenList();
                    if (frozenList.contains(rightClicked.getUniqueId())) {
                        frozenList.remove(rightClicked.getUniqueId());
                        e.getPlayer().sendMessage(ChatColor.GREEN + "You have unfrozen " + rightClicked.getDisplayName());
                        rightClicked.sendMessage(ChatColor.GREEN + "You have been unfrozen");
                    } else {
                        frozenList.add(rightClicked.getUniqueId());
                        rightClicked.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getInstance().getConfig().getString("freezeMessage")));
                        e.getPlayer().sendMessage(ChatColor.RED + "You have frozen " + rightClicked.getDisplayName());
                    }
                } else if (name.equalsIgnoreCase(ITEM_INFO)) {
                    Inventory infoInv = Bukkit.createInventory(null, 9, rightClicked.getDisplayName() + "'s Info");
                    ItemStack compass = new ItemStack(Material.COMPASS);
                    ItemMeta infoMeta = compass.getItemMeta();
                    infoMeta.setDisplayName(ChatColor.RED + "Info");
                    ArrayList<String> lore = new ArrayList<>();
                    lore.add(ChatColor.YELLOW + "Display Name: " + ChatColor.RESET + rightClicked.getDisplayName());
                    lore.add(ChatColor.YELLOW + "UUID: " + rightClicked.getUniqueId());
                    lore.add(ChatColor.YELLOW + "Gamemode: " + ChatColor.GRAY + rightClicked.getGameMode());
                    lore.add(ChatColor.YELLOW + "Opped: " + (rightClicked.isOp() ? ChatColor.GREEN + "Yes" : ChatColor.RED + "No"));
                    if (e.getPlayer().isOp()) {
                        lore.add(ChatColor.YELLOW + "IP: " + rightClicked.getAddress().getAddress().getHostAddress());
                    } else {
                        lore.add(ChatColor.YELLOW + "IP: Hidden");
                    }
                    infoMeta.setLore(lore);
                    compass.setItemMeta(infoMeta);
                    lore.clear();
                    infoInv.setItem(0, compass);
                    ItemStack diamondSword = new ItemStack(Material.DIAMOND_SWORD);
                    infoMeta = diamondSword.getItemMeta();
                    infoMeta.setDisplayName(ChatColor.RED + "Stats");
                    lore.add(ChatColor.YELLOW + "Health: " + ChatColor.RED + rightClicked.getHealth() + ChatColor.DARK_GRAY + "/20.0");
                    lore.add(ChatColor.YELLOW + "Hunger: " + ChatColor.GOLD + rightClicked.getFoodLevel() + ChatColor.DARK_GRAY + "/20.0");
                    lore.add(ChatColor.YELLOW + "Kills/Deaths: " + ChatColor.GREEN + rightClicked.getStatistic(Statistic.PLAYER_KILLS) + ChatColor.DARK_GRAY + "/" + ChatColor.RED + rightClicked.getStatistic(Statistic.DEATHS));
                    lore.add(ChatColor.YELLOW + "Mined Diamonds: " + ChatColor.AQUA + rightClicked.getStatistic(Statistic.MINE_BLOCK, Material.DIAMOND_ORE));
                    infoMeta.setLore(lore);
                    diamondSword.setItemMeta(infoMeta);
                    lore.clear();
                    infoInv.setItem(4, diamondSword);
                    ItemStack exit = new ItemStack(Material.BARRIER);
                    infoMeta = exit.getItemMeta();
                    infoMeta.setDisplayName(ChatColor.RED + "Exit");
                    exit.setItemMeta(infoMeta);
                    infoInv.setItem(8, exit);
                    e.getPlayer().openInventory(infoInv);
                }
            }
        }
    }
    //endregion

    //region Inventory Click Event
    @EventHandler
    public void onInventoryDrag(InventoryClickEvent e) {
        int slot = e.getSlot();
        if (staffList.contains(e.getWhoClicked().getUniqueId()) && (slot == 0 || slot == 4 || slot == 8) && !e.getWhoClicked().hasPermission("staff.creative")) {
            e.setCancelled(true);
        } else if (e.getInventory().getTitle().contains("'s Info") || (!e.getWhoClicked().isOp() && e.getInventory().getTitle().equalsIgnoreCase("Inventory"))) {
            if (slot == 8) {
                e.getWhoClicked().closeInventory();
            } else {
                e.setCancelled(true);
            }
        }
    }
    //endregion

    //region Item Pickup Event
    @EventHandler
    public void onItemPickup(PlayerPickupItemEvent e) {
        if (staffList.contains(e.getPlayer().getUniqueId())) {
            e.setCancelled(true);
        }
    }
    //endregion

}
