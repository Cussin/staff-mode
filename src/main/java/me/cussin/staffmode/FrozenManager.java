package me.cussin.staffmode;

import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.*;

import java.util.ArrayList;
import java.util.UUID;

public class FrozenManager implements Listener {

    @Getter
    private ArrayList<UUID> frozenList = new ArrayList<>();
    private Main plugin = Main.getInstance();

    public boolean isFrozen(Player p) {
        return frozenList.contains(p.getUniqueId());
    }


    //region Player Move Event
    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e) {
        if (isFrozen(e.getPlayer())) {
            e.setTo(e.getFrom());
            e.getPlayer().sendMessage(ChatColor.RED + "You may not move while frozen!");
        }
    }
    //endregion

    //region Player Damage Event
    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player && isFrozen((Player) e.getDamager())) {
            e.getDamager().sendMessage(ChatColor.RED + "You may not attack while frozen!");
        } else if ((e.getEntity() instanceof Player) && frozenList.contains(e.getEntity().getUniqueId())) {
            e.getDamager().sendMessage(ChatColor.RED + "This person is frozen, you may not attack them!");
        }
    }
    //endregion

    //region Player Chat Event
    @EventHandler
    public void onPlayerChat(PlayerCommandPreprocessEvent e) {
        if (isFrozen(e.getPlayer())) {
            for (String s : plugin.getConfig().getStringList("allowedFrozenCommands")) {
                if (e.getMessage().startsWith("/" + s)) {
                    return;
                }
            }
            e.setCancelled(true);
            e.getPlayer().sendMessage(ChatColor.RED + "You may not speak while frozen");
        }
    }
    //endregion

    //region Player Interact Event
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (isFrozen(e.getPlayer())) {
            e.setCancelled(true);
        }
    }
    //endregion

    //region Block Break Event
    @EventHandler
    void onPlayerBreakBlock(BlockBreakEvent e) {
        if (isFrozen(e.getPlayer())) {
            e.setCancelled(true);
        }
    }
    //endregion

    //region Item Drop Event
    @EventHandler
    public void onItemDrop(PlayerDropItemEvent e) {
        if (isFrozen(e.getPlayer())) {
            e.setCancelled(true);
        }
    }
    //endregion

    //region Item Pickup Event
    @EventHandler
    public void onItemPickup(PlayerPickupItemEvent e) {
        if (isFrozen(e.getPlayer())) {
            e.setCancelled(true);
        }
    }
    //endregion

    //region Block Place Event
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        if (isFrozen(e.getPlayer())) {
            e.setCancelled(true);
        }
    }
    //endregion

}
